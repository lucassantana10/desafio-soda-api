class LocationsController < ApplicationController
    before_action :get_current_user_locations, only: [:index, :list, :map]
    before_action :set_location, only: [:show]
    STATUS_SUCCESS = 200
    STATUS_ERROR = 401
    MAP = 'Map'
    LIST = 'List'

    def index        
        render json: {
            user_locations: @user_locations,    
            status: STATUS_SUCCESS
        }
    end

    def list
        list = Location.locations_by_type(@current_user, 'List')
        render json: {
            user_locations: @user_locations,
            list: list,
            status: STATUS_SUCCESS
        }        
    end

    def map
        map = Location.locations_by_type(@current_user, 'Map')
        render json: {
            user_locations: @user_locations,
            map: map,
            status: STATUS_SUCCESS
        }           
    end

    def show
        render json: {
            location: @location,
            status: STATUS_SUCCESS
        }
    end

    def create
        location = Location.create_object(@current_user, params)

        if(location.save)
            render json: {
                location: location,
                message: "Location created successful.",
                status: STATUS_SUCCESS
            }
        else
            render json: {
                message: "Something unexpected happened.",
                status: STATUS_ERROR
            }
        end

    end

    private

    def set_location
        @location = Location.find_by_id(params[:id])
    end

    def get_current_user_locations
        @user_locations = Location.current_user_locations @current_user
    end
end
