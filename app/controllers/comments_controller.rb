class CommentsController < ApplicationController
    STATUS_SUCCESS = 200
    STATUS_ERROR = 401

    def create
        comment = Comment.new(comment_params)
        comment.user_id = params[:user_id]
        comment.location_id = params[:location_id]
        rating = Rating.new(rating_params)        
 
        if comment.save 
            rating.comment_id = comment.id
            rating.save
            
            render json: {
                comment: comment,
                rating: rating,
                message: "Comment and Rating created successful.",
                status: STATUS_SUCCESS
            }
        else
            render json: {
                message: "Something unexpected happened.",
                status: STATUS_ERROR
            }            
        end
    end



    private

    def comment_params
        params.require(:comment).permit(:text)
                 
    end
    def rating_params
        params.require(:rating).permit(:stars)        
    end

end
