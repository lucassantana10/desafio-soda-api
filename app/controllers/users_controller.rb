class UsersController < ApplicationController
    before_action :get_user, only: [:update, :show]
    STATUS_SUCCESS = 200
    STATUS_ERROR = 401

    def index
        users = User.order('created_at DESC').page(params[:page]).per_page(10)

        if(users)
            render json: {
                users: users,
                total: users.count,
                limit: 10,
                page: 1,
                pages: users.total_pages,
                status: STATUS_SUCCESS
            }
        else
            render json: {
                status: STATUS_ERROR
            }            
        end
    end

    def show

        if(@user)
            render json: {
                user: @user,
                status: STATUS_SUCCESS
            }
        else
            render json: {
                status: STATUS_ERROR
            }            
        end        
        
    end

    def update

        if(@user.update(update_params))
            render json: {
                user: @user,
                status: STATUS_SUCCESS
            }
        else
            render json: {
                status: STATUS_ERROR
            }            
        end  
    end


    private

    def get_user
        @user = User.find(params[:id])
    end 

    def update_params
        params.require(:user).permit(:email, :password, :password_confirmation)
    end

end