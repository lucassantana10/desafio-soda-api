class Rating < ApplicationRecord
  belongs_to :comment

  validates :stars, presence: true
  validates_presence_of :comment
end
