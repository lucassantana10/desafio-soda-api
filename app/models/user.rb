class User < ApplicationRecord
    has_secure_password
    has_many :locations
    has_many :ratings

    validates_presence_of :email
    validates_uniqueness_of :email
end
