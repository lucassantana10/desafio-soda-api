class Location < ApplicationRecord
    extend Geocoder::Model::ActiveRecord
    MAP = 'Map'
    LIST = 'List'

    belongs_to :user
    has_many :comments
    has_many :ratings

    validates :coordinates, presence: true

    attribute :comments

    def self.create_object(current_user, params)
        location = self.new

        results = Geocoder.search(params[:location][:coordinates])

        results.first.coordinates
        location.coordinates = results.first.coordinates unless results.first.nil?
        location.user_id = current_user.id 
        location       
    end

    def self.current_user_locations current_user
        list ||= []
        list ||= {}
        locations = self.where(user_id: current_user.id)
        locations.each do |location|
            list << {id: location.id, city: get_city(location.coordinates), coordinates: location.coordinates }
        end
        list   
    end

    def self.locations_by_type(current_user, type)             
        @locations = self.where.not(user_id: current_user.id)
        @current_user_last_coordinates = current_user.locations.last.coordinates

        show_result(type)
    end

    private 

        def self.calculate_distance location_coordinates, current_user_coordinates
            Geocoder::Calculations.distance_between(location_coordinates, 
                                                    current_user_coordinates)
        end

        def self.get_city coordinates
            results =  Geocoder.search(coordinates)
            results.first.city
        end

        def self.get_list(locations)
            list ||= []
            list ||= {}  
            locations.each do |location|
                city = get_city(location.coordinates)

                list << {id: location.id, city: city, coordinates: location.coordinates}
            end
            list.sort_by {|_,city| city}        
        end

        def self.get_map(locations, current_user_coordinates)
            list ||= []
            list ||= {}               
            locations.each do |location|
                city = get_city(location.coordinates)
                distance = calculate_distance(location.coordinates, 
                    current_user_coordinates)

                list << {id: location.id, city: city, coordinates: location.coordinates, distance: distance }
            end

            list.sort_by {|k| k[:distance] }
        end

        def self.show_result type
            case type
            when MAP
                get_map(@locations, @current_user_last_coordinates)    
            when LIST            
                get_list(@locations) 
            end        
        end
end
