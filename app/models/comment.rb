class Comment < ApplicationRecord
  has_one :rating
  # accepts_nested_attributes_for :ratings, allow_destroy: true

  validates :text, presence: true
  validates :user_id, presence: true
  validates :location_id, presence: true

  attribute :rating
end
