# README
Sejam bem-vindos ao respositório, API feito para desafio da SODA

## Software necessário

- Ruby 2.6.5
- PostgreSQL 11 ou superior
- Postman ou similar

## Instruções de execução

Para fazer o setup geral do projeto:

```
$ bin/setup
```

Para executar os testes:

```
$ bundle exec rspec
```

Para executar o servidor:

```
$ bundle exec rails server
```

# Endpoints!

  - LOGIN SESSION - METHOD -> POST
```
    localhost:3000/sessions 

    {
        "user": {
            "email": "email@example.com",
            "password": "password_here"
        }
    }
  ```
  - CREATE USER - METHOD -> POST
  ```
    localhost:3000/registrations  

    {
        "user": {
            "email": "email@example.com",
            "password": "password_here",
            "password_confirmation": "same_password"
        }
    }    
  ```
  - CURRENT USER SHOW - METHOD -> GET
  ```
    localhost:3000/users/1/
  ```
  - CURRENT USER UPDATE - METHOD -> PUT (Can Update email or password/password_confirmation)
  ```
    localhost:3000/users/:user_id/
    {
        "user": {
            "email": "email@example.com",
            "password": "password_here", 
            "password_confirmation": "same_password"
        }
    }  
  ```
  - CREATE LOCATION - METHOD -> POST (NEEDS LOGIN)
  ```
    localhost:3000/users/:user_id/locations
    {
        "location": {
            "coordinates": "[lat, long]"
        }
    }
  ```
  - GET CURRENT USER LIST - METHOD -> GET (NEEDS LOGIN)
  ```
    localhost:3000/users/:user_id/locations/list
  ```
  - GET CURRENT USER MAP - METHOD -> GET (NEEDS LOGIN)
  ```
    localhost:3000/users/:user_id/locations/map
  ```
  - CREATE COMMENT AND RATING - METHOD -> POST
  ```
    localhost:3000/users/:user_id/locations/:location_id

    {
	"comment": {
		"text": "Text description about coordinates"
	},
	"rating": {
		"stars": 4
	}
}
  ```
  - DESTROY SESSION - METHOD -> DELETE
  ```
    localhost:3000/logout
  ```


#### Projeto Publicado
Tomei a decisão de publicar este projeto em produção, no heroku. Segue o link: 
##### https://desafio-soda-api.herokuapp.com/

### Considerações Finais
Em primeiro lugar venho agradecer a oportunidade de participar do desafio, gostei muito e foi enriquecedor resolver esses problemas. Pode ter certeza que eu me empenhei ao máximo para poder entregar este projeto e foi um prazer imenso faze-lo.
