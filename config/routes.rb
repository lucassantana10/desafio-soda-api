Rails.application.routes.draw do
  resources :sessions, only: [:create]
  resources :registrations, only: [:create]
  resources :users, only: [:index, :show, :update] do    
    get "locations/list", to: "locations#list"
    get "locations/map", to: "locations#map"
    resources :locations, only: [:index, :create, :show] do
      resources :comments, only: [:create]
    end
  end
  delete :logout, to: "sessions#logout"
  get :logged_in, to: "sessions#logged_in"

end
