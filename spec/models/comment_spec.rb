require 'rails_helper'

RSpec.describe Comment, type: :model do
  before do
    @user = User.new(:email => "teste@teste.com", :password => "123456", :password_confirmation => "123456")
    @user.save
    @location = Location.new(:coordinates => "[-8.0373353, -34.8968106]", :user_id => @user.id)
    @location.save
    @comment = Comment.new(:text => "Description about location", :location_id => @location.id, :user_id => @user.id)
    @comment.save
    @rating = Rating.new(:stars => 5, :comment_id => @comment.id)
  end

  context 'Create Comment' do
    it 'cannot create Comment' do
      expect { Comment.create! }.to raise_error(ActiveRecord::RecordInvalid)
      expect { Rating.create! }.to raise_error(ActiveRecord::RecordInvalid)
    end

    it 'validate object to register a Comment' do
      expect(@comment.location_id).to_not eq(nil)
      expect(@comment.user_id).to_not eq(nil)
      expect(@comment.text).to_not eq(nil)
    end  

    it 'validate object to register a Rating' do
      expect(@rating.stars).to_not eq(nil)
      expect(@rating.comment_id).to_not eq(nil)    
    end
  end
end
