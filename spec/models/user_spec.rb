require 'rails_helper'

RSpec.describe User, type: :model do
  
  before do
    @user = User.new(:email => "teste@teste.com", :password => "123456", :password_confirmation => "123456")

  end

  context 'Create User' do
    it 'cannot save user' do   #
      expect { User.create! }.to raise_error(ActiveRecord::RecordInvalid)
    end

    it 'validate datas to registrate a new user' do
    
      expect(@user.email).to_not eq("")
      expect(@user.password).to eq("123456")
      expect(@user.password_confirmation).to eq("123456")
    end    
  end
end
