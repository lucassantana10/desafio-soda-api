require 'rails_helper'

RSpec.describe Location, type: :model do

  before do
    @user = User.new(:email => "teste@teste.com", :password => "123456", :password_confirmation => "123456")
    @user.save
    @location = Location.new(:coordinates => "[-8.0373353, -34.8968106]", :user_id => @user.id)
  end

  context 'Create Location' do
    it 'cannot create location' do   #
      expect { Location.create! }.to raise_error(ActiveRecord::RecordInvalid)
    end

    it 'validate datas to registrate a location' do
    
      expect(@location.user_id).to_not eq(nil)
      expect(@location.coordinates).to_not eq(nil)
    end
  end
end
